package com.dmlozdmr.trainme;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ProfilePage extends Fragment{
    private EditText etName, etSurname, etEmail, etAge, etPassword;
    Button bsave;
    private static final String URL = "http://trainme.net16.net/trainMe/";
    private RequestQueue requestQueue;
    private StringRequest request;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.activity_profilepage, container, false);
        bsave = (Button) v.findViewById(R.id.bsave);
        etName = (EditText) v.findViewById(R.id.etName);
        etSurname = (EditText) v.findViewById(R.id.etSurname);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        etAge = (EditText) v.findViewById(R.id.etAge);
        etPassword = (EditText) v.findViewById(R.id.etPassword);
        requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

        request = new StringRequest(Request.Method.POST, URL + "getuserinfo.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    System.out.print("EREEEEEEEEEEEEEEEEEEN");
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.print("ISIIIIIIIIIIIIM" + jsonObject.getString("name").toString());
                    if (jsonObject.names().get(0).equals("name")) {
                        etName.setText(jsonObject.getString("name"));

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", LoginActivity.etEmail.getText().toString());
                return hashMap;
            }
        };

        requestQueue.add(request);
        return v;

    }






}