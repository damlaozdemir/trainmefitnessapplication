package com.dmlozdmr.trainme;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DisplayFoods extends AppCompatActivity {

    TextView foodnamevalue, calorievalue, proteinvalue, fatvalue, carbohydratevalue;

    private static final String URL = "http://trainme.net16.net/trainme/";
    RequestQueue requestQueue;
    private StringRequest request;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_foods);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Food Information");


        foodnamevalue = (TextView) findViewById(R.id.foodnamevalue);
        calorievalue = (TextView) findViewById(R.id.calorievalue);
        proteinvalue = (TextView) findViewById(R.id.proteinvalue);
        fatvalue = (TextView) findViewById(R.id.fatvalue);
        carbohydratevalue = (TextView) findViewById(R.id.carbohydratevalue);

        foodnamevalue.setText(Diet.foodname);
        calorievalue.setText(Diet.calorie);
        proteinvalue.setText(Diet.protein);
        fatvalue.setText(Diet.fat);
        carbohydratevalue.setText(Diet.carbohydrate);


        requestQueue = Volley.newRequestQueue(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                request = new StringRequest(Request.Method.POST, URL+"adduserexercises.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.names().get(0).equals("success")){
                                Snackbar.make(view, "Added to the programs", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();

                            }else{
                                Toast.makeText(getApplicationContext(), "Error " + jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams()throws AuthFailureError {
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("userid", "1");
                        hashMap.put("exerciseid", "2");
                        hashMap.put("programid", "5");

                        return hashMap;
                    }
                };
                requestQueue.add(request);

            }
        });

    }

}
