#Senior Design Project - TrainMe Fitness and Health Application#

This project's aim is to find the best exercises and diet for the users.
Slide for this project is [here](http://slides.com/damlaozdemir/trainme#/).

##Requirements:##
1.[JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
2.[Android SDK](https://developer.android.com/studio/index.html)
3.[Android Volley Library](https://developer.android.com/studio/projects/android-library.html)

##Guide:##
* Dependencies:
They are configured in app/build.gradle
* Activity Configuration:
In the app/src/main/AndroidManifest.xml file, the activities that are used in the project are needed to be specified.
* Classes:
app/src/main/java/com/dmlozdmr/trainme/ folder contains the whole classes generated.
* Images:
 app/src/main/res/drawable/ consists of the images that are used in 3 different sizes.
* Layouts:
app/src/main/res/layout folder contains the xml files that are used for user interface.